import { WarriorAxe, WarriorSword, WarriorSpear } from './Warrior.js';

let patrocle = new WarriorSword("Patrocle", 50, 100);
let ulysse = new WarriorSpear("Ulysse", 80, 150);
let achille = new WarriorSpear("Achille", 60, 160);
let agamemnon = new WarriorSword("Agamemnon", 60, 130);
let hector = new WarriorAxe("Hector", 70, 110);
let paris = new WarriorSword("Pâris", 40, 100);
let priam = new WarriorAxe("Priam", 50, 110);

// WarriorAxe > WarriorSword
// WarriorSword > WarriorSpear
// WarriorSpear > WarriorAxe

function battle(warrior1, warrior2) {
    while (warrior1.life > 0 && warrior2.life > 0) {
        console.log(warrior1.attack(warrior2));
        console.log(warrior2.attack(warrior1));
        console.log(`Il reste à ${warrior1.name} ${warrior1.life} points de vie`);
        console.log(`Il reste à ${warrior2.name} ${warrior2.life} points de vie`);
    }
    if (warrior1.life < 1 && warrior2.life < 1) {
        return 'Egalité, les deux sont morts.';
    } else if (warrior1.life < 1) {
        return `${warrior2.name} gagne.`;
    } else if (warrior2.life < 1) {
        return `${warrior1.name} gagne.`;
    }
}

console.log(battle(hector, patrocle));