class Warrior {
    constructor(name, power, life) {
        this.name = name;
        this.power = power;
        this.life = life;
    }
    attack(opponent) {
        opponent.life -= this.power;
        return `${this.name} attaque ${opponent.name}!`;
    }
    isAlive() {
        return this.life > 0 ? true + `, ${this.name} est encore en vie.` : false + `, ${this.name} est mort.`;
    }
}

export class WarriorAxe extends Warrior {
    constructor(name, power, life) {
        super(name, power, life);
    }
    attack(opponent) {
        if (opponent instanceof WarriorSword) {
            opponent.life -= this.power * 2;
            return `Avantage! ${this.name} attaque à la hache ${opponent.name}, qui se défend avec une épée !`;
        } else {
            return super.attack(opponent);
        }
    }
}


export class WarriorSword extends Warrior {
    constructor(name, power, life) {
        super(name, power, life);
    }
    attack(opponent) {
        if (opponent instanceof WarriorSpear) {
            opponent.life -= this.power * 2;
            return `Avantage! ${this.name} attaque à l'épée ${opponent.name}, qui se défend avec une lance!`;
        } else {
            return super.attack(opponent);
        }
    }
}

export class WarriorSpear extends Warrior {
    constructor(name, power, life) {
        super(name, power, life);
    }
    attack(opponent) {
        if (opponent instanceof WarriorAxe) {
            opponent.life -= this.power * 2;
            return `Avantage! ${this.name} attaque avec une lance ${opponent.name}, qui se défend avec une hache !`;
        } else {
            return super.attack(opponent);
        }
    }
}


